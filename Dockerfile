FROM node:16.20.2-buster

WORKDIR /app
COPY . . 

# Get Docker build args
ARG CONNECTION_STRING 
ARG NEWS_API_KEY

# Set var. env
ENV CONNECTION_STRING $CONNECTION_STRING
ENV NEWS_API_KEY $NEWS_API_KEY

USER root
RUN yarn install && \
    yarn add dotenv

EXPOSE 3000
CMD ["yarn", "start"]


